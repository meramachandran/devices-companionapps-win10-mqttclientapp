﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using Windows.Networking;
using Windows.Networking.Sockets;

using Org.BouncyCastle.Crypto.Tls;
using Org.BouncyCastle.Security;
namespace MqttClientApp
{
    class SrpNetworkChannel : IMqttNetworkChannel
    {
        // remote host information
        private HostName remoteHostName;
        private int remotePort;
        private string srpuser;
        private string password;

        private Stream secureStream;

        public SrpNetworkChannel(string remoteHostname, int remotePort, string srpuser, string password)
        {
            this.remoteHostName = new HostName(remoteHostname);
            this.remotePort = remotePort;
            this.srpuser = srpuser;
            this.password = password;
        }

        public bool DataAvailable
        {
            get { return true; }
        }

        public void Accept()
        {
            throw new NotImplementedException();
        }

        public void Close()
        {
            secureStream.Dispose();
        }

        public void Connect()
        {
            StreamSocket socket = new StreamSocket();
            socket.ConnectAsync(this.remoteHostName,
                this.remotePort.ToString(),
                SocketProtectionLevel.PlainSocket).AsTask().Wait();

            TlsClientProtocol protocol =
                new TlsClientProtocol(socket.InputStream.AsStreamForRead(),
                    socket.OutputStream.AsStreamForWrite(), new SecureRandom());

            SrpTlsClient client = new SrpTlsClient(Encoding.UTF8.GetBytes(srpuser), Encoding.UTF8.GetBytes(password));
            protocol.Connect(client);
            secureStream = protocol.Stream;
        }

        public int Receive(byte[] buffer)
        {
            return secureStream.Read(buffer, 0, buffer.Length);
        }

        public int Receive(byte[] buffer, int timeout)
        {
            throw new NotImplementedException();
        }

        public int Send(byte[] buffer)
        {
            secureStream.Write(buffer, 0, buffer.Length);
            return buffer.Length;
        }
    }
}
