﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using uPLibrary.Networking.M2Mqtt;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MqttClientApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private MqttClient client;
        private bool connected;
        private string hostname = "15.79.249.109";
        private int port = 1883;
        private string srpuser = "srptest";
        private string password = "monaco";

        public MainPage()
        {
            this.InitializeComponent();
            ConnectToBroker();
        }

        private void ConnectToBroker()
        {
            publish.IsEnabled = false;

            IMqttNetworkChannel channel = new SrpNetworkChannel(hostname, port, srpuser, password);
            client = new MqttClient(hostname,port,channel);
            messageBlock.Text = "Connecting to the broker.  Please wait...";
            IAsyncAction action = ThreadPool.RunAsync(
                (workItem) =>
                {
                    try
                    {
                        client.Connect("Win10App");
                        connected = true;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Caught an exception during connect!");
                        Debug.WriteLine(ex.Message);
                        Debug.WriteLine(ex.StackTrace);
                    }
                });

            action.Completed = new AsyncActionCompletedHandler(
                async (IAsyncAction asyncInfo, AsyncStatus asyncStatus) =>
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                        CoreDispatcherPriority.High,
                        new DispatchedHandler(() =>
                        {
                            if (connected)
                            {
                                messageBlock.Text = "Connected to the broker!  Ready to send messages.";
                                publish.IsEnabled = true;
                            } else
                            {
                                messageBlock.Text = "Failed to connect to the broker!.";
                            }
                        }));
                });
        }

        private void publish_Click(object sender, RoutedEventArgs e)
        {
            string topic = topicInput.Text;
            string message = messageInput.Text;

            if (topic.Length == 0)
            {
                messageBlock.Text = "Topic cannot be empty.  Pleasae specify a topic.";
                return;
            }

            publish.IsEnabled = false;
            messageBlock.Text = "Sending message...";

            IAsyncAction action = ThreadPool.RunAsync(
                (workItem) =>
                {
                    try
                    {
                        client.Publish(topic, Encoding.UTF8.GetBytes(message));
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Caught an exception during publish!");
                        Debug.WriteLine(ex.Message);
                        Debug.WriteLine(ex.StackTrace);
                    }
                });

            action.Completed = new AsyncActionCompletedHandler(
                async (IAsyncAction asyncInfo, AsyncStatus asyncStatus) =>
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                        CoreDispatcherPriority.High,
                        new DispatchedHandler(() =>
                        {
                            messageBlock.Text = "Message sent!";
                            publish.IsEnabled = true;
                        }));
                });


        }
    }
}
